//
//  Server.swift
//  Plex
//
//  Created by Hjalti Jakobsson on 25.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation

class Server {
    
    var host : String
    var port : Int
    
    var url : String {
        get {
            return "http://\(host):\(port)"
        }
    }
    
    init(host: String, port: Int) {
        self.host = host
        self.port = port
    }
    
    func library() -> Library {
        return Library(server: self)
    }
}