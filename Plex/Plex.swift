//
//  Plex.swift
//  Plex
//
//  Created by Hjalti Jakobsson on 25.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation
import Alamofire

var AuthToken : String?

class Plex {
    
    class func open(url: String) {
        
        let URL = NSURL(string: url)!
        let URLRequest = NSMutableURLRequest(URL: URL)
        
        if let authToken = AuthToken {
            URLRequest.setValue(authToken, forHTTPHeaderField: "X-Plex-Token")
        }
        
        let encoding = Alamofire.ParameterEncoding.URL
        
        Alamofire.request(URLRequest).response { (_, _, data, error) -> Void in
            println(data)
        }
    }
    
}