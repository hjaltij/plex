//
//  Library.swift
//  Plex
//
//  Created by Hjalti Jakobsson on 25.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

import Foundation
import AEXML

class Library {
    
    var server : Server
    var url : String {
        get {
            return server.url
        }
    }
    
    init(server: Server) {
        self.server = server
    }
    
    func section(id: String) -> Section? {
        return search_sections(base_doc(), id: id).first
    }
    
    func base_doc() -> AEXMLDocument {
        Plex.open(self.url)
        
        return AEXMLDocument()
    }
    
    func search_sections(doc: AEXMLDocument, id: NSString) -> [Section] {
        return [Section()]
    }
}