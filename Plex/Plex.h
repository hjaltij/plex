//
//  Plex.h
//  Plex
//
//  Created by Hjalti Jakobsson on 25.5.2015.
//  Copyright (c) 2015 Hjalti Jakobsson. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Plex.
FOUNDATION_EXPORT double PlexVersionNumber;

//! Project version string for Plex.
FOUNDATION_EXPORT const unsigned char PlexVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Plex/PublicHeader.h>


